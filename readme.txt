mixin: mixinが入っています。原則mixinはこちらに追加してください。追加するときは機能をコメントアウトで記入すること。
reboot: reset.cssの内容が入っています。common.scssなどにimportしてください。
setting:ここでサイトのテーマカラーとコンテンツ幅などを設定します。各プロジェクトにインポートするときに変数の値を変更します。

mixin: 
Contains mixin. Please add principle mixin here. Please comment out the function when adding it.

reboot: 
Contains the contents of reset.css. Please import into common.scss etc.

setting:
Set the theme color and content width of the site here.
Change the value of the variable when import to each project.